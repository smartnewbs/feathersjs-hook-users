
const errors = require('@feathersjs/errors');

/**
 * Extracts the google profile from the OAuth payload to user properties
 * Requires user service to have the following properties: email, name, avatar
 *
 * @param hook
 * @returns {*}
 */
module.exports = function extractGoogleProfile(hook) {
  if (hook.type !== 'before') {
    throw new Error('The "ExtractGoogleProfile" hook should only be used as a "before" hook.');
  }

  if (hook.method !== 'create') {
    throw new errors.MethodNotAllowed('The "ExtractGoogleProfile" hook should only be used on "create" service method.');
  }

  if (hook.data.hasOwnProperty('google')) {
    hook.data.email = hook.data.google.profile.emails[0].value;
    hook.data.name = hook.data.google.profile.displayName;
    hook.data.avatar = hook.data.google.profile.photos[0].value;
  }

  return hook;
};
