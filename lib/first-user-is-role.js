
const errors = require('@feathersjs/errors');

/**
 * First user created will get the specified role
 *
 * @param role
 * @returns {Function}
 */
module.exports = function firstUserIsRole(role) {
  return function (hook) {
    if (hook.type !== 'before') {
      throw new Error('The "FirstUserIsRole" hook should only be used as a "before" hook.');
    }

    if (hook.method !== 'create') {
      throw new errors.MethodNotAllowed('The "FirstUserIsRole" hook should only be used for the "create" method.');
    }

    // If it was an internal call then skip this hook
    if (!hook.params.provider) {
      return hook;
    }

    // Set provider as undefined so we avoid an infinite loop if this hook is set on the resource we are requesting.
    return hook.app.service('users').find({ provider: undefined }).then(result => {

      if (result.total === 0) {
        hook.data.role = role;
      }

      return hook;
    });
  };
};
