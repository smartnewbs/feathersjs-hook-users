
const errors = require('@feathersjs/errors');

/**
 * Restrict property change by roles
 * Requires the user service to have a role property
 *
 * @param property
 * @param roles
 * @throws RestrictPropertyByRoles
 * @returns {*}
 */
module.exports = function restrictPropertyByRoles(property, roles) {
  if (typeof property !== 'string') {
    throw new Error('The "RestrictPropertyByRoles" hook requires first parameter to be a string that represent a property');
  }

  if (!Array.isArray(roles)) {
    throw new Error('The "RestrictPropertyByRoles" hook requires second parameter to be an array of roles');
  }

  return function (hook) {
    // If it was an internal call then skip this hook
    if (!hook.params.provider) {
      return hook;
    }

    if (hook.type !== 'before') {
      throw new Error('The "RestrictPropertyByRoles" hook should only be used as a "before" hook');
    }

    if (['create', 'update', 'patch'].indexOf(hook.method) === -1 || (!hook.id && ['update', 'patch'].indexOf(hook.method) !== -1)) {
      throw new errors.MethodNotAllowed('The "RestrictPropertyByRoles" hook should only be used on "create", "update" or "patch" service method');
    }

    // If we are not trying to create, update or patch the protected property
    if (!hook.hasOwnProperty('data') || !hook.data.hasOwnProperty(property)) {
      return hook;
    }
    // If the method is "create" and we have the protected property in payload
    else if (hook.method === 'create') {
      throw new errors.Forbidden(`You do not have the required role to create ${property} property`);
    }

    // If user is not authenticated stop here, only check if the user is logged in for "update" & "patch"
    if (!hook.params.user && ['update', 'patch'].indexOf(hook.method) !== -1) {
      throw new errors.NotAuthenticated();
    }

    // look up the document and skip if the property did not change
    // Set provider as undefined so we avoid an infinite loop if this hook is set on the resource we are requesting.
    const params = Object.assign({}, hook.params, { provider: undefined });
    return hook.service.get(hook.id, params).then(entry => {
      if (entry.toJSON) {
        entry = entry.toJSON();
      } else if (entry.toObject) {
        entry = entry.toObject();
      }

      // If the property's value did not change we do not need to check for user's role
      if (entry && entry.hasOwnProperty(property) && entry[property] === hook.data[property]) {
        return hook;
      }

      // If user does not have the proper role to modify this property
      if (roles.indexOf(hook.params.user.role) === -1) {
        throw new errors.Forbidden(`You do not have the required role to modify ${property} property`);
      }

      return hook;
    });
  };
};
