
const assert = require('assert');
const { extractGoogleProfile } = require('../lib');

let hookBefore = {};

describe('Extract Google Profile', () => {
  beforeEach(() => {

    hookBefore = {
      type: 'before',
      method: 'create',
      params: {
        user: { role: 'ROLE_SALE', status: 'STATUS_BANNED' },
        payload: { userId: 1000 }
      },
      data: {
        role: 'ROLE_ADMIN',
        status: 'STATUS_ACTIVE',
        email: 'test',
        google: {
          profile: {
            displayName: 'Bobby Jones',
            emails: [ {value: 'bobby@jones.com'} ],
            photos: [ {value: 'myavatar.png'} ]
          }
        }
      },
      id: 1000,
    };
  });

  it('Should throw Error if hook type is not before', () => {
    hookBefore.type = 'after';

    assert.throws(() => {
      extractGoogleProfile(hookBefore);
    }, Error);
  });

  it('Should throw @feathersjs/errors.MethodNotAllowed if method is not create', () => {
    hookBefore.method = 'patch';

    assert.throws(() => {
      extractGoogleProfile(hookBefore);
    }, Error);
  });

  it('Should do nothing if no google payload', () => {
    delete hookBefore.data.google;

    const hook = extractGoogleProfile(hookBefore);
    assert.deepEqual(hook, hookBefore);
  });

  it('Should associate profile fields to users hook.data properties', () => {
    const hook = extractGoogleProfile(hookBefore);
    assert.equal(hook.data.name, hookBefore.data.google.profile.displayName);
    assert.equal(hook.data.email, hookBefore.data.google.profile.emails[0].value);
    assert.equal(hook.data.avatar, hookBefore.data.google.profile.photos[0].value);
  });

});
