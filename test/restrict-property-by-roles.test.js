
const assert = require('assert');
const sinon = require('sinon');
const errors = require('@feathersjs/errors');
const { restrictPropertyByRoles } = require('../lib');

let hookBefore = {};
describe('Restrict Property By Roles', () => {
  beforeEach(() => {

    hookBefore = {
      type: 'before',
      method: 'update',
      params: {
        provider: 'provider',
        user: {role: 'ROLE_UNIQUE', status: 'STATUS_BANNED', email: 'test'},
        payload: {userId: 1000}
      },
      data: {role: 'ROLE_ADMIN', status: 'STATUS_ACTIVE', email: 'test'},
      id: 1000,
    };
  });

  it('Should throw Error if first argument is null', () => {
    assert.throws(() => {
      restrictPropertyByRoles(null, ['ROLE_UNIQUE'])(hookBefore);
    }, Error);
  });

  it('Should throw Error if first argument is undefined', () => {
    assert.throws(() => {
      restrictPropertyByRoles(undefined, ['ROLE_UNIQUE'])(hookBefore);
    }, Error);
  });

  it('Should throw Error if second argument is null', () => {
    assert.throws(() => {
      restrictPropertyByRoles('email', null)(hookBefore);
    }, Error);
  });

  it('Should throw Error if second argument is undefined', () => {
    assert.throws(() => {
      restrictPropertyByRoles('email', undefined)(hookBefore);
    }, Error);
  });

  it('Should throw Error if hook type is not before', () => {
    hookBefore.type = 'after';
    assert.throws(() => {
      restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
    }, Error);
  });

  it('Should not check role if provider is undefined', () => {
    delete hookBefore.params.provider;

    const hook = restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
    assert.deepEqual(hook, hookBefore);
  });

  it('Should not check role if we are not trying to change data', () => {
    delete hookBefore.data;

    const hook = restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
    assert.deepEqual(hook, hookBefore);
  });

  it('Should not check role if we are not trying to change the protected property', () => {
    delete hookBefore.data.email;

    const hook = restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
    assert.deepEqual(hook, hookBefore);
  });

  it('Should throw Error if user role does not match', () => {
    assert.throws(() => {
      restrictPropertyByRoles('email', ['ROLE_ADMIN'])(hookBefore);
    }, Error);
  });

  it('Should throw Error if user role is undefined', () => {
    delete hookBefore.params.user.role;

    assert.throws(() => {
      restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
    }, Error);
  });

  it('Should throw @feathersjs/errors.NotAuthenticated if not authenticated for method "update" or "patch"', () => {
    delete hookBefore.params.user;

    const methods = ['update', 'patch'];
    for (let i=0; i < methods.length; i++) {
      hookBefore.method = methods[i];

      assert.throws(() => {
        restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
      }, errors.NotAuthenticated);
    }
  });

  it('Should NOT throw @feathersjs/errors.NotAuthenticated if not authenticated for method "create"', () => {
    delete hookBefore.params.user;
    delete hookBefore.data.email;
    hookBefore.method = 'create';

    assert.doesNotThrow(() => {
      const hook = restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
      assert.deepEqual(hook, hookBefore);
    }, errors.NotAuthenticated);
  });

  it('Should throw @feathersjs/errors.MethodNotAllowed if no hook.id & method "update" or "patch"', () => {
    delete hookBefore.id;

    const methods = ['update', 'patch'];
    for (let i=0; i < methods.length; i++) {
      hookBefore.method = methods[i];

      assert.throws(() => {
        restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
      }, errors.MethodNotAllowed);
    }
  });

  it('Should NOT throw @feathersjs/errors.MethodNotAllowed if no hook.id & method "create"', () => {
    delete hookBefore.id;
    delete hookBefore.data.email;
    hookBefore.method = 'create';

    assert.doesNotThrow(() => {
      const hook = restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
      assert.deepEqual(hook, hookBefore);
    }, errors.MethodNotAllowed);
  });

  it('Should throw @feathersjs/errors.Forbidden if protected property is in hook.data and method "create"', () => {
    hookBefore.method = 'create';

    assert.throws(() => {
      restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
    }, errors.Forbidden);
  });

  describe('Retrieves current entry', () => {
    beforeEach(() => {
      hookBefore.service = mockService({role: 'ROLE_ADMIN', status: 'STATUS_ACTIVE', email: 'newtest'});
    });

    it('Should throw @feathersjs/errors.MethodNotAllowed if method not create, update or patch', async () => {
      try {
        hookBefore.method = 'remove';
        await restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
        assert.fail('Should of thrown throw @feathersjs/errors.MethodNotAllowed');
      } catch (err) {
        assert.ok(err instanceof errors.MethodNotAllowed);
      }

      delete hookBefore.data.email;
      const methods = ['create', 'update', 'patch'];
      for (let i=0; i < methods.length; i++) {
        hookBefore.method = methods[i];

        const hook = await restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
        assert.deepEqual(hook, hookBefore);
      }
    });

    it('Should throw @feathersjs/errors.Forbidden if user role NOT does match, single', async () => {
      try {
        await restrictPropertyByRoles('email', ['ROLE_INVALID'])(hookBefore);
        assert.fail('Should of thrown throw @feathersjs/errors.Forbidden');
      } catch (err) {
        assert.ok(err instanceof errors.Forbidden);
      }
    });

    it('Should throw @feathersjs/errors.Forbidden if user role NOT does match, multiple', async () => {
      try {
        await restrictPropertyByRoles('email', ['ROLE_INVALID1', 'ROLE_INVALID2', 'ROLE_INVALID3'])(hookBefore);
        assert.fail('Should of thrown throw @feathersjs/errors.Forbidden');
      } catch (err) {
        assert.ok(err instanceof errors.Forbidden);
      }
    });

    it('Should NOT throw Error if user role does match, single', async () => {
      const hook = await restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
      assert.deepEqual(hook, hookBefore);
    });

    it('Should NOT throw Error if user role does match, multiple', async () => {
      const hook = await restrictPropertyByRoles('email', ['ROLE_ADMIN', 'ROLE_UNIQUE', 'ROLE_OTHER'])(hookBefore);
      assert.deepEqual(hook, hookBefore);
    });

    it('Should not check role if property did not change value', async () => {
      hookBefore.service = mockService({role: 'ROLE_ADMIN', status: 'STATUS_ACTIVE', email: 'test'});

      const hook = await restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
      assert.deepEqual(hook, hookBefore);
    });

    it('Should check service.get parameters to ensure provider:undefined', async () => {
      hookBefore.service = mockService({role: 'ROLE_ADMIN', status: 'STATUS_ACTIVE', email: 'test'});
      sinon.spy(hookBefore.service, 'get');

      await restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
      const args = hookBefore.service.get.args[0];
      assert.equal(hookBefore.service.get.callCount, 1);
      assert.equal(args.length, 2);
      assert.equal(args[0], hookBefore.id);
      const params = Object.assign({}, hookBefore.params, { provider: undefined });
      assert.deepEqual(args[1], params);
    });

    it('Should call the toJSON on the entry if it exists', async () => {
      const entry = {
        role: 'ROLE_ADMIN',
        status: 'STATUS_ACTIVE',
        email: 'test',
        toJSON () {
          return { role: this.role, status: this.status, email: this.email };
        }
      };
      sinon.spy(entry, 'toJSON');

      hookBefore.service = mockService(entry);

      await restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
      assert.ok(entry.toJSON.called);
    });

    it('Should call the toObject on the entry if it exists', async () => {
      const entry = {
        role: 'ROLE_ADMIN',
        status: 'STATUS_ACTIVE',
        email: 'test',
        toObject () {
          return { role: this.role, status: this.status, email: this.email };
        }
      };
      sinon.spy(entry, 'toObject');

      hookBefore.service = mockService(entry);

      await restrictPropertyByRoles('email', ['ROLE_UNIQUE'])(hookBefore);
      assert.ok(entry.toObject.called);
    });

  });

});

function mockService(result) {
  return {
    get () {
      return new Promise(resolve => resolve(result));
    }
  };
}
