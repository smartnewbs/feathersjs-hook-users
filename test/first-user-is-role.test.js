
const assert = require('assert');
const errors = require('@feathersjs/errors');
const { firstUserIsRole } = require('../lib');

let hookBefore = {};
describe('First User Is Role', () => {
  beforeEach(() => {

    hookBefore = {
      type: 'before',
      method: 'create',
      params: {
        provider: 'provider',
        user: {role: 'ROLE_SALE', status: 'STATUS_BANNED'},
        payload: {userId: 1000}
      },
      data: {role: 'ROLE_ADMIN', status: 'STATUS_ACTIVE', email: 'test'},
      id: 1000,
    };
  });

  it('Should throw Error if hook type is not before', async () => {
    hookBefore.type = 'after';

    try {
      hookBefore.app = mockService({total: 0});
      await firstUserIsRole('ROLE_FIRST')(hookBefore);
      assert.fail('Should of thrown Error');
    } catch (err) {
      assert.ok(err instanceof Error);
    }
  });

  it('Should throw @feathersjs/errors.MethodNotAllowed if method is not create', async () => {
    hookBefore.method = 'patch';

    try {
      hookBefore.app = mockService({total: 0});
      await firstUserIsRole('ROLE_FIRST')(hookBefore);
      assert.fail('Should of thrown @feathersjs/errors.MethodNotAllowed');
    } catch (err) {
      assert.ok(err instanceof errors.MethodNotAllowed);
    }
  });

  it('Should do nothing if provider is undefined', async () => {
    delete hookBefore.params.provider;

    hookBefore.app = mockService({total: 0});
    let hook = await firstUserIsRole('ROLE_FIRST')(hookBefore);
    assert.equal(hook.data.role, 'ROLE_ADMIN');
  });

  it('Should assign role if first user', async () => {
    hookBefore.app = mockService({total: 0});
    let hook = await firstUserIsRole('ROLE_FIRST')(hookBefore);
    assert.equal(hook.data.role, 'ROLE_FIRST');
  });

  it('Should NOT assign role if NOT first user', async () => {
    hookBefore.app = mockService({total: 1});
    let hook = await firstUserIsRole('ROLE_FIRST')(hookBefore);
    assert.equal(hook.data.role, 'ROLE_ADMIN');
  });

  it('Should NOT assign role if total is -1', async () => {
    hookBefore.app = mockService({total: -1});
    let hook = await firstUserIsRole('ROLE_FIRST')(hookBefore);
    assert.equal(hook.data.role, 'ROLE_ADMIN');
  });

  it('Should NOT assign role if total is null', async () => {
    hookBefore.app = mockService({total: null});
    let hook = await firstUserIsRole('ROLE_FIRST')(hookBefore);
    assert.equal(hook.data.role, 'ROLE_ADMIN');
  });

  it('Should NOT assign role if total is not defined', async () => {
    hookBefore.app = mockService({});
    let hook = await firstUserIsRole('ROLE_FIRST')(hookBefore);
    assert.equal(hook.data.role, 'ROLE_ADMIN');
  });

});

function mockService(result) {
  return {
    service() {
      return {
        find() {
          return new Promise(resolve => resolve(result));
        }
      };
    }
  };
}
