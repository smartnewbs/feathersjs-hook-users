
# @smartnewbs/feathersjs-hook-users
![License](https://img.shields.io/npm/l/@smartnewbs/feathersjs-hook-users.svg)
![Version](https://img.shields.io/npm/v/@smartnewbs/feathersjs-hook-users.svg)
![Node](https://img.shields.io/node/v/@smartnewbs/feathersjs-hook-users.svg)
[![Coverage Status](https://coveralls.io/repos/bitbucket/smartnewbs/feathersjs-hook-users/badge.svg?branch=master)](https://coveralls.io/bitbucket/smartnewbs/feathersjs-hook-users?branch=master)

This lib is for [Feathersjs](https://github.com/feathersjs/feathers) as utility hooks.

## Installation
```bash
npm install @smartnewbs/featherjs-hook-users --save
```

## firstUserIsRole
The hook will modify `hook.data.role` with the specified role only if its the first entry in the `users` service.
It is meant to be only used as a before create hook in your `users.hooks.js`:
```javascript
const { firstUserIsRole } = require('@smartnewbs/feathersjs-hook-users');
module.exports = {
  before: {
    create: [ firstUserIsRole('ROLE_ADMIN') ]
  }
}
```

## restrictPropertyByRoles
The hook will check the authenticated user's role (`hook.params.user.role`) against the roles
specified. It is meant to be only used as a before hook for the methods create, update and patch.  
```javascript
const { restrictPropertyByRoles } = require('@smartnewbs/feathersjs-hook-users');
module.exports = {
  before: {
    create: [ restrictPropertyByRoles('status', ['ROLE_ADMIN']) ],
    update: [ restrictPropertyByRoles('status', ['ROLE_ADMIN']) ],
    patch: [ restrictPropertyByRoles('status', ['ROLE_ADMIN']) ]
  }
}
```

## extractGoogleProfile
The hook is mean to extract Google OAuth profile and associate the properties to the user model.
```javascript
hook.data.email = hook.data.google.profile.emails[0].value;
hook.data.name = hook.data.google.profile.displayName;
hook.data.avatar = hook.data.google.profile.photos[0].value;
```
It is meant to be a before hook for the method create.
```javascript
const { extractGoogleProfile } = require('@smartnewbs/feathersjs-hook-users');
module.exports = {
  before: {
    create: [ extractGoogleProfile ]
  }
}
```
